import React, { Component } from 'react';
import {
  Grid, Col, Row,
} from 'react-bootstrap';
import { styles } from './Footer.style';

/** CUSTOM COMPONENTS **/
import { Logo } from './../Logo/Logo';
export class Footer extends Component{
  render(){
    let year = new Date();
    year = year.getFullYear();
    return (
      <footer className="footer" style={styles.FooterContainer}>
        <Grid style={styles.FooterGrid}>
          <Row>
            <Col xs={12} md={12}>
              <Logo footer />
              <Grid>
                <span style={{color:'#FFF',fontFamily:"'Roboto Condensed',sans-serif",fontSize:10,fontWeight:500,textTransform:'uppercase',textAlign:'center',letterSpacing:2,display:'block'}}>Acreditar Eventos &copy; {year} - Todos os direitos reservados.</span>
              </Grid>
            </Col>

            {/*
            <Col xs={12} md={2}>
              <a className={'assinatura'} title={'Desenvolvido por iDrops'} href={'http://www.dropspublicidade.com.br'} target={'_blank'} />
            </Col> */}
          </Row>
        </Grid>
      </footer>
    );
  }
}
