import ReactCSS from 'reactcss';
export const styles = ReactCSS({
  'default': {
    FooterContainer: {
      display: 'block',
      position: 'relative',
      backgroundColor: 'transparent',
      padding: '20px 0',
      zIndex: 7
    },
    FooterGrid: {
      backgroundColor: 'transparent'
    }
  }
})
